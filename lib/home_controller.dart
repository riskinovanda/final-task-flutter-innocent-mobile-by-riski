import 'package:get/get.dart';
import 'package:task_4_state_management/model/detail_articles_model.dart';
import 'package:task_4_state_management/model/home_articles_model.dart';
import 'package:task_4_state_management/service/app_service.dart';

class HomeController extends GetxController with AppService {
  var headlineListData = HomeArticlesModel().headline;
  var latestNewsListData = HomeArticlesModel().latestNews;
  var editorChoiceListData = HomeArticlesModel().editorChoice;
  var topicListData = HomeArticlesModel().topic;
  var sliderPage = 0;

  //for detail
  var articleListData = <DetailArticle>[];
  var articleDetailData = DetailArticle();
  var contentSelectedId = "0";
  var articleRelatedListData = <DetailArticle>[];

  HomeController() {
    fetchDataHomeArticles();
    fetchDataDetailArticles();
  }

  setSliderPage(value) {
    sliderPage = value;
    update();
  }

  setContentId(value) {
    contentSelectedId = value;
    getDetailArticle();
    getDetailArticleRelatedList();
    update();
  }

  @override
  void onInit() {
    fetchDataHomeArticles();
    fetchDataDetailArticles();
    super.onInit();
  }

  fetchDataHomeArticles() async {
    try {
      final result = await getDataHomeArticles();
      headlineListData = result.headline;
      latestNewsListData = result.latestNews;
      editorChoiceListData = result.editorChoice;
      addTopic();
      update();
    } catch (error) {
      print(error);
    }
  }

  addTopic() {
    topicListData = <String>[];

    for (int i = 0; i < headlineListData.length; i++) {
      bool isMatch = false;
      for (int j = 0; j < topicListData.length; j++) {
        if (headlineListData[i].topic == topicListData[j]) isMatch = true;
      }
      if (!isMatch) {
        topicListData.add(headlineListData[i].topic);
      }
    }

    for (int i = 0; i < latestNewsListData.length; i++) {
      bool isMatch = false;
      for (int j = 0; j < topicListData.length; j++) {
        if (latestNewsListData[i].topic == topicListData[j]) isMatch = true;
      }
      if (!isMatch) {
        topicListData.add(latestNewsListData[i].topic);
      }
    }

    for (int i = 0; i < editorChoiceListData.length; i++) {
      bool isMatch = false;
      for (int j = 0; j < topicListData.length; j++) {
        if (editorChoiceListData[i].topic == topicListData[j]) isMatch = true;
      }
      if (!isMatch) {
        topicListData.add(editorChoiceListData[i].topic);
      }
    }
  }

  fetchDataDetailArticles() async {
    try {
      final result = await getDataDetailArticles();
      articleListData = result.data;
      update();
    } catch (error) {
      print(error);
    }
  }

  getDetailArticle() {
    for (int i = 0; i < articleListData.length; i++) {
      if (articleListData[i].id == contentSelectedId)
        articleDetailData = articleListData[i];
    }
  }

  getDetailArticleRelatedList() {
    articleRelatedListData = List<DetailArticle>();
    for (int i = 0; i < articleListData.length; i++) {
      if (articleListData[i].topic == articleDetailData.topic)
        articleRelatedListData.add(articleListData[i]);
    }
  }

  customDateFormat(String date) {
    if (date != null && date.isNotEmpty) {
      String customDate = date.substring(8, 10);
      switch (date.substring(5, 7)) {
        case "01":
          customDate += " Januari";
          break;
        case "02":
          customDate += " Februari";
          break;
        case "03":
          customDate += " Maret";
          break;
        case "04":
          customDate += " April";
          break;
        case "05":
          customDate += " Mei";
          break;
        case "06":
          customDate += " Juni";
          break;
        case "07":
          customDate += " Juli";
          break;
        case "08":
          customDate += " Agustus";
          break;
        case "09":
          customDate += " September";
          break;
        case "10":
          customDate += " Oktober";
          break;
        case "11":
          customDate += " November";
          break;
        case "12":
          customDate += " Desember";
          break;
      }
      customDate += " " + date.substring(0, 4) + ", ";
      customDate += date.substring(11, 16);

      return customDate;
    } else
      return "";
  }
}
