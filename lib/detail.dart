import 'package:better_player/better_player.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_4_state_management/home_controller.dart';
import 'package:video_player/video_player.dart';

class Detail extends StatelessWidget {
  HomeController controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          leading: Container(),
          backgroundColor: Colors.white,
          elevation: 3.0,
          flexibleSpace: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Image.asset(
                'assets/logo/logo_celebritiesdotid.png',
                width: 120,
              ),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.all(12.0),
              child: Align(
                alignment: Alignment.bottomRight,
                child: Text(
                  "Foto",
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
              child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  controller.articleDetailData?.title ?? "",
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20.0),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      controller.articleDetailData?.creator ?? "",
                      style: TextStyle(fontSize: 12.0, color: Colors.orange),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    controller.customDateFormat(
                        controller.articleDetailData?.date ?? ""),
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                controller.articleDetailData.image == null
                    ? CircularProgressIndicator()
                    : Image.network(controller.articleDetailData.image.thumb),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      "Foto: " + controller.articleDetailData?.image.source ??
                          "",
                      style: TextStyle(fontSize: 8.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 32.0,
                ),
                Text(
                  controller.articleDetailData?.articleContent ?? "",
                  style: TextStyle(fontSize: 14.0),
                  textAlign: TextAlign.justify,
                ),
                SizedBox(
                  height: 20.0,
                ),
                controller.articleDetailData.video != null
                    ? Container(
                        height: 200,
                        child: BetterPlayer.network(
                          controller.articleDetailData?.video ?? "",
                          betterPlayerConfiguration:
                              BetterPlayerConfiguration(aspectRatio: 16 / 9),
                        ))
                    : SizedBox(
                        height: 2.0,
                      ),
                SizedBox(
                  height: 40.0,
                ),
                Divider(
                  height: 1,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Topik Terkait",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                controller.topicListData == null
                    ? CircularProgressIndicator()
                    : Align(
                        alignment: Alignment.topLeft,
                        child: Wrap(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(16)),
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1,
                                    )),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child:
                                      Text(controller.articleDetailData.topic),
                                ),
                              ),
                            )
                          ],
                        )),
                SizedBox(
                  height: 20.0,
                ),
                Divider(
                  height: 1,
                  color: Colors.red,
                ),
                SizedBox(
                  height: 20.0,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Artikel Terkait",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: controller.articleRelatedListData?.length ?? 0,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        controller.setContentId(
                            controller.articleRelatedListData[index].id);
                        Get.toNamed('/detail');
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.centerLeft,
                          width: 200.0,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.network(controller
                                    .articleRelatedListData[index].image.thumb),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    controller
                                        .articleRelatedListData[index].topic,
                                    style: TextStyle(
                                        color: Colors.red[600], fontSize: 14),
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 14, 10, 10),
                                child: Text(
                                    controller
                                        .articleRelatedListData[index].title,
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                    )),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    controller.customDateFormat(controller
                                        .articleRelatedListData[index].date),
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 14, 10, 10),
                                child: Divider(
                                  color: Colors.red,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 80.0,
                ),
              ],
            ),
          )),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              color: Color(0xFFededed),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Container(
                          width: 40,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/logo/home3.png',
                                height: 14,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'Home',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Container(
                          width: 40,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/logo/menu.png',
                                height: 14,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'Menu',
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Container(
                          width: 40,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/logo/search.png',
                                height: 14,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'Search',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      onTap: () {
                        Get.back();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Container(
                          width: 40,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/logo/circle-left.png',
                                height: 14,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'Back',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Container(
                          width: 40,
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/logo/share.png',
                                height: 14,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'Share',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
