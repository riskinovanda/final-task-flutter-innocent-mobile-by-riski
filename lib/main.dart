import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:task_4_state_management/detail.dart';
import 'package:task_4_state_management/home.dart';
import 'package:task_4_state_management/home_controller.dart';

void main() {
  Get.put(HomeController());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page: () => Home(),
        ),
        GetPage(
          name: '/detail',
          page: () => Detail(),
        )
      ],
      home: Home(),
    );
  }
}
