import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:task_4_state_management/home_controller.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        initState: (state) => HomeController().fetchDataHomeArticles(),
        builder: (controller) {
          return Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(90),
              child: AppBar(
                backgroundColor: Colors.white,
                elevation: 3.0,
                flexibleSpace: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset(
                      'assets/logo/logo_celebritiesdotid.png',
                      scale: 20 / 9,
                    ),
                  ),
                ),
              ),
            ),
            body: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Headline',
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Divider(
                        height: 3,
                        color: Colors.red[600],
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      controller.headlineListData == null
                          ? CircularProgressIndicator()
                          : Container(
                              height: 400,
                              child: CarouselSlider.builder(
                                itemCount:
                                    controller.headlineListData?.length ?? 0,
                                itemBuilder: (BuildContext context, int index,
                                    int realIndex) {
                                  return InkWell(
                                    onTap: () {
                                      controller.setContentId(controller
                                          .headlineListData[
                                              controller.sliderPage]
                                          .id);
                                      Get.toNamed('/detail');
                                    },
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 30, 10, 30),
                                          child: Image.network(controller
                                              .headlineListData[index]
                                              .image
                                              .thumb),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 0, 10, 0),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              controller.headlineListData[index]
                                                  .topic,
                                              style: TextStyle(
                                                color: Colors.red[600],
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              10, 20, 10, 0),
                                          child: Text(
                                            controller
                                                .headlineListData[index].title,
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.black),
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                },
                                options: CarouselOptions(
                                  autoPlay: true,
                                  autoPlayAnimationDuration:
                                      Duration(seconds: 1),
                                  autoPlayInterval: Duration(seconds: 3),
                                  initialPage: controller.sliderPage,
                                  disableCenter: true,
                                  height: 100,
                                  onPageChanged: (index, _) {
                                    controller.setSliderPage(index);
                                  },
                                ),
                              ),
                            ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(
                            controller.headlineListData?.length ?? 0,
                            (index) => Container(
                              width: controller.sliderPage == index ? 30 : 18,
                              height: 4,
                              margin: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 4.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: controller.sliderPage == index
                                    ? Colors.red[600]
                                    : Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40.0,
                      ),
                      Container(
                        height: 330.0,
                        color: Colors.black,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 16.0,
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: 16.0,
                                ),
                                Text(
                                  'Editor\'s Choice',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20.0),
                                ),
                                SizedBox(
                                  width: 16.0,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 16.0,
                            ),
                            Expanded(
                              child: ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    controller.editorChoiceListData?.length ??
                                        0,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      controller.setContentId(controller
                                          .editorChoiceListData[index].id);
                                      Get.toNamed('/detail');
                                    },
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      width: 200.0,
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Image.network(controller
                                                .editorChoiceListData[index]
                                                .image
                                                .thumb),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 10, 10, 0),
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                controller
                                                    .editorChoiceListData[index]
                                                    .topic,
                                                style: TextStyle(
                                                    color: Colors.red[600],
                                                    fontSize: 10),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 10, 10, 0),
                                            child: Text(
                                              controller
                                                  .editorChoiceListData[index]
                                                  .title,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 10, 10, 0),
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                controller.customDateFormat(
                                                    controller
                                                        .editorChoiceListData[
                                                            index]
                                                        .date),
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 10,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Hot Topics',
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Divider(
                        height: 3,
                        color: Colors.red[600],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      controller.topicListData == null
                          ? CircularProgressIndicator()
                          : Padding(
                              padding:
                                  const EdgeInsets.only(right: 20, left: 20),
                              child: Wrap(
                                  children: controller.topicListData
                                      .map((v) => Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  16)),
                                                  border: Border.all(
                                                    color: Colors.black,
                                                    width: 1,
                                                  )),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(v),
                                              ),
                                            ),
                                          ))
                                      .toList()),
                            ),
                      SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Latest News',
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Divider(
                        height: 3,
                        color: Colors.red[600],
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: controller.latestNewsListData?.length ?? 0,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              controller.setContentId(
                                  controller.latestNewsListData[index].id);
                              Get.toNamed('/detail');
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                alignment: Alignment.centerLeft,
                                width: 200.0,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Image.network(controller
                                          .latestNewsListData[index]
                                          .image
                                          .thumb),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 10, 10, 0),
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          controller
                                              .latestNewsListData[index].topic,
                                          style: TextStyle(
                                              color: Colors.red[600],
                                              fontSize: 14),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 14, 10, 10),
                                      child: Text(
                                          controller
                                              .latestNewsListData[index].title,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black,
                                          )),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 10, 10, 0),
                                      child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          controller.customDateFormat(controller
                                              .latestNewsListData[index].date),
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10, 14, 10, 10),
                                      child: Divider(
                                        color: Colors.red,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: 120,
                      )
                    ],
                  ),
                ),
                Positioned(
                  bottom: 4,
                  left: 4,
                  right: 4,
                  child: Card(
                    color: Colors.white,
                    shadowColor: Colors.black,
                    elevation: 16,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(
                            height: 3,
                            width: 32,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.grey),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/home3.png',
                                          color: Colors.red,
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Home',
                                          style: TextStyle(
                                              color: Colors.red,
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/menu.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Menu',
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/film.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Movie',
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/fire.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Hot Topic',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/search.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Search',
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/star-empty.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'Popular',
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/info.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'About Us',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 30),
                              InkWell(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)),
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 50,
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/logo/grid.png',
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Text(
                                          'More',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }
}
