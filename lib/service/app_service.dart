import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:task_4_state_management/model/detail_articles_model.dart';
import 'package:task_4_state_management/model/home_articles_model.dart';

class AppService {
  // Future<ArticlesModel> getDataArticles() async {
  //   var header = {'Content-Type': 'application/json; charset=UTF-8'};
  //   final response = await http.get(
  //       Uri.parse('https://private-1518a-celebrities.apiary-mock.com/celeb'),
  //       headers: header);

  //   if (response.statusCode == 200) {
  //     return ArticlesModel.fromJson(jsonDecode(response.body));
  //   } else {
  //     throw Exception('failed');
  //   }
  // }

  Future<HomeArticlesModel> getDataHomeArticles() async {
    var header = {'Content-Type': 'application/json; charset=UTF-8'};
    final response = await http.get(
        Uri.parse(
            'https://605ada0627f0050017c05533.mockapi.io/celebrities/home'),
        headers: header);

    if (response.statusCode == 200) {
      var responseFinal = jsonDecode(response.body);
      return HomeArticlesModel.fromJson(responseFinal[0]);
    } else {
      throw Exception('failed');
    }
  }

  Future<DetailArticlesModel> getDataDetailArticles() async {
    var header = {'Content-Type': 'application/json; charset=UTF-8'};
    final response = await http.get(
        Uri.parse(
            'https://605ada0627f0050017c05533.mockapi.io/celebrities/Detail-article'),
        headers: header);

    if (response.statusCode == 200) {
      var responseFinal = jsonDecode(response.body);
      return DetailArticlesModel.fromJson(responseFinal);
    } else {
      throw Exception('failed');
    }
  }
}
