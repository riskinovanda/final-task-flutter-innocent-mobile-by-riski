class HomeArticlesModel {
  String message;
  List<News> latestNews;
  List<News> headline;
  List<News> editorChoice;
  List<String> topic;

  HomeArticlesModel(
      {this.message, this.latestNews, this.headline, this.editorChoice});

  HomeArticlesModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    if (json['latest_news'] != null) {
      latestNews = new List<News>();
      json['latest_news'].forEach((v) {
        latestNews.add(new News.fromJson(v));
      });
    }
    if (json['headline'] != null) {
      headline = new List<News>();
      json['headline'].forEach((v) {
        headline.add(new News.fromJson(v));
      });
    }
    if (json['editor_choice'] != null) {
      editorChoice = new List<News>();
      json['editor_choice'].forEach((v) {
        editorChoice.add(new News.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.latestNews != null) {
      data['latest_news'] = this.latestNews.map((v) => v.toJson()).toList();
    }
    if (this.headline != null) {
      data['headline'] = this.headline.map((v) => v.toJson()).toList();
    }
    if (this.editorChoice != null) {
      data['editor_choice'] = this.editorChoice.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class News {
  String id;
  String title;
  String date;
  Image image;
  String topic;

  News({this.id, this.title, this.date, this.image, this.topic});

  News.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    date = json['date'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    topic = json['topic'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['date'] = this.date;
    if (this.image != null) {
      data['image'] = this.image.toJson();
    }
    data['topic'] = this.topic;
    return data;
  }
}

class Image {
  String thumb;
  String source;

  Image({this.thumb, this.source});

  Image.fromJson(Map<String, dynamic> json) {
    thumb = json['thumb'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumb'] = this.thumb;
    data['source'] = this.source;
    return data;
  }
}
