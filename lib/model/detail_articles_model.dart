class DetailArticlesModel {
  List<DetailArticle> data;

  DetailArticlesModel({this.data});

  DetailArticlesModel.fromJson(List<dynamic> json) {
    if (json != null) {
      data = List<DetailArticle>();
      for (int i = 0; i < json.length; i++) {
        var detailArticle = DetailArticle.fromJson(json[i]);
        data.add(detailArticle);
      }
    }
  }
}

class DetailArticle {
  String id;
  String title;
  String creator;
  String date;
  Image image;
  String articleContent;
  String video;
  String topic;

  DetailArticle(
      {this.id,
      this.title,
      this.creator,
      this.date,
      this.image,
      this.articleContent,
      this.video,
      this.topic});

  DetailArticle.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    creator = json['creator'];
    date = json['date'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    articleContent = json['article_content'];
    video = json['video'];
    topic = json['topic'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['creator'] = this.creator;
    data['date'] = this.date;
    if (this.image != null) {
      data['image'] = this.image.toJson();
    }
    data['article_content'] = this.articleContent;
    data['video'] = this.video;
    data['topic'] = this.topic;
    return data;
  }
}

class Image {
  String thumb;
  String source;

  Image({this.thumb, this.source});

  Image.fromJson(Map<String, dynamic> json) {
    thumb = json['thumb'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['thumb'] = this.thumb;
    data['source'] = this.source;
    return data;
  }
}
