class ArticlesModel {
  String status;
  String message;
  List<Data> data;

  ArticlesModel({this.status, this.message, this.data});

  ArticlesModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int channelId;
  String title;
  String thumbLarge;
  String datePublish;
  String description;
  String tipe;

  Data(
      {this.channelId,
      this.title,
      this.thumbLarge,
      this.datePublish,
      this.description,
      this.tipe});

  Data.fromJson(Map<String, dynamic> json) {
    channelId = json['channel_id'];
    title = json['title'];
    thumbLarge = json['thumb_large'];
    datePublish = json['date_publish'];
    description = json['description'];
    tipe = json['tipe'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['channel_id'] = this.channelId;
    data['title'] = this.title;
    data['thumb_large'] = this.thumbLarge;
    data['date_publish'] = this.datePublish;
    data['description'] = this.description;
    data['tipe'] = this.tipe;
    return data;
  }
}
